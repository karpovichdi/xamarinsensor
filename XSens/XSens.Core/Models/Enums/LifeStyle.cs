﻿namespace XSens.Core.Models.Enums
{
    public enum LifeStyle
    {
        Singleton = 0,
        Transient = 1,
        Scoped = 2
    }
}
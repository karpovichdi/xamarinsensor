﻿using XSens.Core.IoC;
using XSens.Core.Models.Enums;

namespace XSens.Core.DependencyResolver
{
    public static class ComponentRegistry
    {
        public static IContainerService Container { get; set; }

        public static void Register<T, TN>(LifeStyle lifeStyle) where T : class where TN : class, T
        {
            Container.Register<T, TN>(lifeStyle);
        }
    }
}
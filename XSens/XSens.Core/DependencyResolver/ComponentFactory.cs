﻿using System;

namespace XSens.Core.DependencyResolver
{
    public static class ComponentFactory
    {
        public static T Resolve<T>() where T : class
        {
            return ComponentRegistry.Container.Resolve<T>();
        }

        public static object Resolve(Type type)
        {
            return ComponentRegistry.Container.Resolve(type);
        }
    }
}
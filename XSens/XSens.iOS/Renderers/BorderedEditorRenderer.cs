﻿using System;
using Foundation;
using XSens.iOS.Renderers;
using XSens.Views;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BorderedEditor), typeof(BorderedEditorRenderer))]
namespace XSens.iOS.Renderers
{
    public class BorderedEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control == null || e.NewElement == null) return;

            var formsControl = (BorderedEditor)e.NewElement;

            Control.Layer.BorderWidth = formsControl.BorderWidth;
            Control.Layer.CornerRadius = formsControl.CornerRadius;
            Control.Layer.BorderColor = formsControl.BorderColor.ToCGColor();
            Control.Layer.BackgroundColor = formsControl.BackgroundColor.ToCGColor();

            Control.TextContainer.MaximumNumberOfLines = (nuint)formsControl.MaxLines;
            Control.TextContainer.LineBreakMode = UIKit.UILineBreakMode.TailTruncation;
            Control.EnablesReturnKeyAutomatically = false;
        }

        protected override bool ShouldChangeText(UITextView textView, NSRange range, string text)
        {
            if (text == "\n")
            {
                return false;
            }

            return base.ShouldChangeText(textView, range, "/ln");
        }
    }
}
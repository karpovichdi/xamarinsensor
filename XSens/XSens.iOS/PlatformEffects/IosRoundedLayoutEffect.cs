﻿using System.Linq;
using XSens.iOS.PlatformEffects;
using XSens.PlatformEffects;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("12")]
[assembly: ExportEffect(typeof(IosRoundedLayoutEffect), nameof(RoundedLayoutEffect))]
namespace XSens.iOS.PlatformEffects
{
    public class IosRoundedLayoutEffect : PlatformEffect
    {
        private RoundedLayoutEffect _effect;

        protected override void OnAttached()
        {
            _effect = (RoundedLayoutEffect)Element.Effects.FirstOrDefault(x => x is RoundedLayoutEffect);

            if (Control != null)
            {
                Control.Layer.CornerRadius = _effect.CornerRadius;

                if (_effect.HasShadow)
                {
                    Control.Layer.ShadowColor = _effect.ShadowColor.ToCGColor();
                    Control.Layer.ShadowOffset = new CoreGraphics.CGSize(0, 0);
                    Control.Layer.ShadowRadius = _effect.ShadowRadius;
                    Control.Layer.ShadowOpacity = 1.0f;
                }

                if (_effect.HasBorder)
                {
                    SetBorder(Control);
                }

                Control.Layer.MasksToBounds = false;
            }
            else if (Container is UIView view)
            {
                view.Layer.CornerRadius = _effect.CornerRadius;

                if (_effect.HasShadow)
                {
                    view.Layer.ShadowColor = _effect.ShadowColor.ToCGColor();
                    view.Layer.ShadowOffset = new CoreGraphics.CGSize(0, 0);
                    view.Layer.ShadowRadius = _effect.ShadowRadius;
                    view.Layer.ShadowOpacity = 1.0f;
                }

                if (_effect.HasBorder)
                {
                    SetBorder(view);
                }

                view.Layer.MasksToBounds = false;
            }
        }

        private void SetBorder(UIView view)
        {
            view.Layer.BorderWidth = _effect.BorderWidth;
            view.Layer.BorderColor = _effect.BorderColor.ToCGColor();
        }

        protected override void OnDetached()
        {

        }
    }
}
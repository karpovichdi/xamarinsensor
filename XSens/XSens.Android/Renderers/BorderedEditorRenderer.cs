﻿using Android.Content;
using Android.Graphics.Drawables;
using XSens.Views;
using XSens.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Android.OS;


[assembly: ExportRenderer(typeof(BorderedEditor), typeof(BorderedEditorRenderer))]
namespace XSens.Droid.Renderers
{
    public class BorderedEditorRenderer : EditorRenderer
    {
        public BorderedEditorRenderer(Context context) : base(context) { }


        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control == null || e.NewElement == null) return;

            var formsControl = (BorderedEditor)e.NewElement;
            var backgroundShape = new GradientDrawable();

            backgroundShape.SetStroke((int)formsControl.BorderWidth * 3, formsControl.BorderColor.ToAndroid());
            backgroundShape.SetColor(formsControl.BackgroundColor.ToAndroid());
            backgroundShape.SetCornerRadius(formsControl.CornerRadius * 3);

            Control.SetLines(formsControl.MaxLines);
            Control.SetMaxLines(formsControl.MaxLines);
            Control.SetSingleLine(true);
            Control.SetBackground(backgroundShape);
        }
    }
}
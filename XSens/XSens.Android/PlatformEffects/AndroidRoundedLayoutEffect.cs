﻿using System.Linq;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using XSens.Droid.PlatformEffects;
using XSens.PlatformEffects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using AColor = Android.Graphics.Color;
using AView = Android.Views.View;

[assembly: ResolutionGroupName("12")]
[assembly: ExportEffect(typeof(AndroidRoundedLayoutEffect), nameof(RoundedLayoutEffect))]
namespace XSens.Droid.PlatformEffects
{
    public class AndroidRoundedLayoutEffect : PlatformEffect
    {
        private float _density;

        private RoundedLayoutEffect _effect;

        protected override void OnAttached()
        {
            _density = Android.App.Application.Context.Resources.DisplayMetrics.Density;
            _effect = (RoundedLayoutEffect)Element.Effects.FirstOrDefault(x => x is RoundedLayoutEffect);

            if (Control != null)
            {
                Control.OutlineProvider = new RoundedOutlineProvider(_effect.CornerRadius * _density);
                Control.ClipToOutline = true;

                if (_effect.HasShadow)
                {
                    Control.Elevation = _effect.ShadowRadius * _density;
                }

                if (_effect.HasBorder)
                {
                    SetBorder(Control);
                }

                //Control.SetOutlineAmbientShadowColor(effect.ShadowColor.ToAndroid());
                //Control.SetOutlineSpotShadowColor(effect.ShadowColor.ToAndroid());
            }
            else if (Container != null)
            {
                Container.OutlineProvider = new RoundedOutlineProvider(_effect.CornerRadius * _density);
                Container.ClipToOutline = true;

                if (_effect.HasShadow)
                {
                    Container.Elevation = _effect.ShadowRadius * _density;
                }

                if (_effect.HasBorder)
                {
                    SetBorder(Container);
                }

                //Container.SetOutlineAmbientShadowColor(effect.ShadowColor.ToAndroid());
                //Container.SetOutlineSpotShadowColor(effect.ShadowColor.ToAndroid());
            }
        }

        private void SetBorder(AView v)
        {
            var gd = new GradientDrawable();
            gd.SetShape(ShapeType.Rectangle);
            gd.SetCornerRadius(_effect.CornerRadius * _density);
            gd.SetStroke((int)(_effect.BorderWidth * _density), AColor.ParseColor(_effect.BorderColor.ToHex()));
            v.SetBackground(gd);
        }

        protected override void OnDetached()
        {

        }

        public class RoundedOutlineProvider : ViewOutlineProvider
        {
            private readonly float _radius;

            public RoundedOutlineProvider(float radius)
            {
                _radius = radius;
            }

            public override void GetOutline(AView view, Outline outline)
            {
                var r = (int)_radius;
                outline?.SetRoundRect(0, 0, view.Width, view.Height, _radius);
            }
        }
    }
}

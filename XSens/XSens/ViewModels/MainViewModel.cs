﻿using System.Threading;
using System.Threading.Tasks;

namespace XSens.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        protected readonly CancellationTokenSource CancellationToken;

        private bool _isConnected = true;
        public bool IsConnected
        {
            get => _isConnected;
            set
            {
                _isConnected = value;
                OnPropertyChanged(nameof(IsConnected));
            }
        }

        protected async Task OnBackButtonClicked()
        {
            CancellationToken.Cancel();

            await NavigationService.GoBackAsync(null);
        }

        public virtual Task InitializeAsync(object parameter)
        {
            return Task.CompletedTask;
        }

        public virtual Task NavigatedBackAsync(object parameter)
        {
            return Task.CompletedTask;
        }

        public virtual Task WillGoBackAsync()
        {
            return Task.CompletedTask;
        }
    }
}

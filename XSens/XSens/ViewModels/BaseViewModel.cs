﻿using System.Threading;
using System.Threading.Tasks;
using XSens.Core.DependencyResolver;
using XSens.Services.Interfaces;
using Xamarin.Forms;

namespace XSens.ViewModels
{
    public class BaseViewModel : BindableObject
    {
        protected readonly INavigationService NavigationService;

        protected readonly CancellationTokenSource CancellationToken;

        public BaseViewModel()
        {
            NavigationService = ComponentFactory.Resolve<INavigationService>();
        }

        public virtual Task InitializeAsync(object parameter)
        {
            //IsConnected = Connectivity.NetworkAccess == NetworkAccess.Internet;

            return Task.CompletedTask;
        }

        public virtual Task NavigatedBackAsync(object parameter)
        {
            return Task.CompletedTask;
        }

        public virtual Task WillGoBackAsync()
        {
            //Connectivity.ConnectivityChanged -= Connectivity_ConnectivityChanged;

            return Task.CompletedTask;
        }

        protected async Task OnBackButtonClicked()
        {
            CancellationToken?.Cancel();
        }
    }
}

﻿using Xamarin.Forms;

namespace XSens.Views
{
    public class BorderedEditor : Editor
    {
        public static readonly BindableProperty BorderWidthProperty = BindableProperty.Create(
            propertyName: nameof(BorderWidth),
            returnType: typeof(float),
            declaringType: typeof(BorderedEditor),
            defaultValue: default(float),
            defaultBindingMode: BindingMode.Default);

        public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(
            propertyName: nameof(CornerRadius),
            returnType: typeof(float),
            declaringType: typeof(BorderedEditor),
            defaultValue: default(float),
            defaultBindingMode: BindingMode.Default);

        public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(
            propertyName: nameof(BorderColor),
            returnType: typeof(Color),
            declaringType: typeof(BorderedEditor),
            defaultValue: Color.Gray,
            defaultBindingMode: BindingMode.Default);

        public new static readonly BindableProperty BackgroundColorProperty = BindableProperty.Create(
            propertyName: nameof(BackgroundColor),
            returnType: typeof(Color),
            declaringType: typeof(BorderedEditor),
            defaultValue: Color.Black,
            defaultBindingMode: BindingMode.Default);

        public static readonly BindableProperty MaxLinesProperty = BindableProperty.Create(
            propertyName: nameof(MaxLines),
            returnType: typeof(int),
            declaringType: typeof(BorderedEditor),
            defaultValue: 1,
            defaultBindingMode: BindingMode.Default);

        public BorderedEditor()
        {
            TextChanged += (sender, e) =>
            {
                InvalidateMeasure();
            };
        }

        public float BorderWidth
        {
            get => (float)this.GetValue(BorderWidthProperty);
            set => SetValue(BorderWidthProperty, value);
        }

        public float CornerRadius
        {
            get => (float)this.GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        public Color BorderColor
        {
            get => (Color)this.GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }

        public new Color BackgroundColor
        {
            get => (Color)this.GetValue(BackgroundColorProperty);
            set => SetValue(BackgroundColorProperty, value);
        }

        public int MaxLines
        {
            get => (int)this.GetValue(MaxLinesProperty);
            set => SetValue(MaxLinesProperty, value);
        }
    }
}
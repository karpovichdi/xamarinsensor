﻿using XSens.Core.DependencyResolver;
using XSens.Services.Interfaces;
using Xamarin.Forms;
using Application = Xamarin.Forms.Application;

namespace XSens
{
    public partial class App : Application
    {
        protected readonly INavigationService _navigationService;

        public App()
        {
            Resolver.RegisterDependencies();

            _navigationService = ComponentFactory.Resolve<INavigationService>();

            InitializeComponent();

            _navigationService.InitializeAsync();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

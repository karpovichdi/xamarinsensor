﻿using Xamarin.Forms;

namespace XSens.Utils.Style
{
    public static class Colors
    {
        static Colors()
        {
            PrimatyColorLight = (Color)Application.Current.Resources["PrimatyColorLight"];
        }

        public static Color PrimatyColorLight { get; set; }
    }
}
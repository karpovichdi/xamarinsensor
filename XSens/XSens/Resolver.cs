﻿using XSens.Core.IoC;
using XSens.Core.Models.Enums;
using XSens.Services.Interfaces;
using XSens.ViewModels;
using XSens.Services.Navigation;
using static XSens.Core.DependencyResolver.ComponentRegistry;

namespace XSens
{
    public class Resolver
    {
        public static void RegisterDependencies()
        {
            Container = new SimpleInjectorContainerService();

            RegisterServices();
            RegisterViewModels();
        }

        private static void RegisterServices()
        {
            Container.Register<INavigationService, NavigationService>();
        }

        private static void RegisterViewModels()
        {
            Container.Register<MainViewModel>(LifeStyle.Transient);
        }
    }
}

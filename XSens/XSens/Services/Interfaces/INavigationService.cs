﻿using System.Threading.Tasks;
using XSens.ViewModels;

namespace XSens.Services.Interfaces
{
    public interface INavigationService
    {
        Task InitializeAsync();

        Task NavigateToAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel;

        Task NavigateToAsync<TViewModel>(object parameter, bool animated = true) where TViewModel : BaseViewModel;

        void RemovePreviousPage();

        void ClearNavigationStack();

        Task GoBackAsync(object parameter, bool animated = true);
    }
}
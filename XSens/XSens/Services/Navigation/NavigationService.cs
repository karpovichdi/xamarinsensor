﻿using System;
using System.Linq;
using System.Threading.Tasks;
using XSens.Services.Interfaces;
using Xamarin.Forms;
using XSens.ViewModels;
using XSens.Pages;
using System.Reflection;
using System.Globalization;

namespace XSens.Services.Navigation
{
    public class NavigationService : INavigationService
    {
        public Task InitializeAsync()
        {
            return NavigateToAsync<MainViewModel>();
        }

        public Task NavigateToAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), null, animated);
        }

        public Task NavigateToAsync<TViewModel>(object parameter, bool animated = true) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter, animated);
        }

        public void ClearNavigationStack()
        {
            if (Application.Current.MainPage is NavigationPage navigationPage)
            {
                var existingPages = navigationPage.Navigation.NavigationStack.Take(navigationPage.Navigation.NavigationStack.Count - 1).ToList();
                foreach (var page in existingPages)
                {
                    navigationPage.Navigation.RemovePage(page);
                }
            }
        }

        public void RemovePreviousPage()
        {
            if (Application.Current.MainPage is NavigationPage navigationPage)
            {
                if (navigationPage.Navigation.NavigationStack.Count > 1)
                {
                    var page = navigationPage.Navigation.NavigationStack[navigationPage.Navigation.NavigationStack.Count - 2];
                    navigationPage.Navigation.RemovePage(page);
                }
            }
        }

        public async Task GoBackAsync(object parameter, bool animated = true)
        {
            if (Application.Current.MainPage is NavigationPage mainPage)
            {
                await (mainPage.CurrentPage.BindingContext as BaseViewModel).WillGoBackAsync();
                await mainPage.PopAsync(animated);
                await (mainPage.CurrentPage.BindingContext as BaseViewModel).NavigatedBackAsync(parameter);
            }
        }

        private async Task InternalNavigateToAsync(Type viewModelType, object parameter, bool animated)
        {
            Page page = CreatePage(viewModelType, parameter);

            if (page is MainPage)
            {
                Application.Current.MainPage = new NavigationPage(page);
            }
            else
            {
                if (Application.Current.MainPage is NavigationPage navigationPage)
                {
                    await navigationPage.PushAsync(page, animated);
                }
                else
                {
                    Application.Current.MainPage = new NavigationPage(page);
                }
            }

            (page.BindingContext as BaseViewModel).InitializeAsync(parameter);
        }

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            var viewName = viewModelType.FullName.Replace("ViewModel", "Page");
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewModelAssemblyName);
            var viewType = Type.GetType(viewAssemblyName);
            return viewType;
        }

        private Page CreatePage(Type viewModelType, object parameter)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);
            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType) as Page;
            return page;
        }
    }
}
